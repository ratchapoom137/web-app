import React from 'react';
import { Route, Switch } from 'react-router-dom';
import LoginPage from './Login';
import MainPage from './Main';

function Routes() {
  return (
    <div style={{ width: '100%' }}>
      <Switch>
        {' '}
        {/* ต้องใส่ switch มาคอบ route เพราะว่าไม่งั้น component จะซ้อนกัน ต้องใส่ switch เพื่อสลับไปอีกหน้า */}
        <Route exact path="/" component={LoginPage} />
        <Route component={MainPage} />
      </Switch>
    </div>
  );
}

export default Routes;
