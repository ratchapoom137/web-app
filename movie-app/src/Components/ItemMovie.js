import React from 'react';
import { Card } from 'antd';
import TextTruncate from 'react-text-truncate';
import { connect } from 'react-redux';
const { Meta } = Card;

const mapDispatchToProps = dispatch => {
  return {
    onItemMovieClick: item =>
      dispatch({
        type: 'click_item',
        payload: item
      })
  };
};

// redux เข้ามาช่วยเรื่องของ cycle prop
function ItemMovie(props) {
  // prop เรามาสามารถกำหนด prop ได้ว่าจะเอา object แบบไหนเข้ามาได้บ้างโดยใช้ prop-types
  const item = props.item;
  return (
    <Card
      onClick={() => {
        props.onItemMovieClick(item);
      }}
      hoverable
      cover={<img src={item.image_url} />}
    >
      <Meta
        title={item.title}
        description={
          <TextTruncate
            line={1}
            truncateText="..."
            text={item.overview}
            textTruncateChild={<a href="#">Read more</a>}
          />
        }
      />
    </Card>
  );
}

export default connect(
  null,
  mapDispatchToProps
)(ItemMovie);
